#include <map>
#include <iostream>
#include <cstdlib>
#include <pqxx/pqxx>
#include <string>
#include <chrono>
#include <thread>
using namespace std;
using namespace pqxx;

result run_command(string &sql, connection * c) {
  work W(*c);
  result R(W.exec(sql));
  W.commit();
  return R;
}

void insert_order(connection * c, int ordernum, int userid, 	\
		  int id, string description, int amount, 	\
		  int whid, int desx, int desy) {
   string sql = "INSERT INTO SHOP_AORDER " \
     "(ordernum, USERID, \"ID\", DESCRIPTION, AMOUNT, WHID, DESX, DESY, truck_ready, pack_ready, load_ready, delivered, email)" \
     "VALUES ( " + to_string(ordernum) + ", " + to_string(userid) + ", " + to_string(id) + ", '" \
     + description + "', " + to_string(amount) + ", "+ to_string(whid) + ", " + to_string(desx) + ", " \
     + to_string(desy) + ", FALSE, FALSE, FALSE, FALSE, '414227934@qq.com'" + ");";
   run_command(sql, c);
   
  
}

void insert_stock(connection * c, int i ) {
  string sql = " INSERT INTO shop_stock (gid, whid, amount)"
    " VALUES ( " + to_string(i) + ", 0, 0);" ;
  run_command(sql, c);
}
void delete_database(connection * c) {
  string sql = "delete from shop_stock; delete from shop_aorder; delete from shop_uorder;";
  run_command(sql, c);
}
int main(void) {
  srand(time(NULL));
  connection * c = new connection("host=vcm-3223.vm.duke.edu port=9999 dbname=postgres user=postgres");
  // delete_database(c);
  
  int goodamount;
  int whamount;
  char a;
  cout << "please enter good amount\n";
  cin >> goodamount;
  cout << "please enter warehouse amount\n";
  cin >> whamount;
  cout << "enter a to start\n";
  cin >> a;
  map<int, string> good;
  for (int i = 0; i <goodamount; i++) {
    good[i] = 'a' + i;
    insert_stock(c, i);
  }
  int ordernum = 1;
  int testnum = 0;
  while(testnum < 100) {
    int id = rand() % goodamount;
    int userid = rand() % 100;
    string description = good[id];
    int amount = rand() % 100;
    int whid = rand() % whamount;
    int desx = rand() % 2000 - 1000;
    int desy = rand() % 2000 - 1000;
    insert_order(c, ordernum, userid , 	  \
		 id, description, amount, \
		 whid, desx, desy);
    ordernum++;
    testnum++;
    std::this_thread::sleep_for (std::chrono::seconds(1));
  }
}
