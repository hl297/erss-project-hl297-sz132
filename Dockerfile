FROM ubuntu:16.04
RUN mkdir /code
ADD . /code/
WORKDIR /code

RUN ls -a && apt-get update && apt-get install -y autoconf automake libtool curl make g++ unzip git libpqxx-dev
RUN cd protobuf-3.5.1 && ./configure && make && make check && make install && ldconfig
RUN make


